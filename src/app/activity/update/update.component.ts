import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityService } from 'src/app/service/activity/activity-service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})

export class UpdateComponent implements OnInit {
  form = this.fb.group({
    id: [''],
    name: [''],
    location: [''],
    host: [''],
    startPeriodRegis: [''],
    endPeriodRegis: [''],
    startDate: [''],
    endDate: [''],
    time: [''],
    description: ['']
  });

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private acivityService: ActivityService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.acivityService.getActivityById(params['actId'])
          .subscribe(activity => {
            this.form = this.fb.group({
              id: activity.id,
              name: activity.name,
              location: activity.location,
              host: activity.host,
              startPeriodRegis: activity.startPeriodRegis,
              endPeriodRegis: activity.endPeriodRegis,
              startDate: activity.startDate,
              endDate: activity.endDate,
              time: activity.time,
              description: activity.description
            });
          });
      });
  }

  update() {
    console.log("updated!")
  }

}
