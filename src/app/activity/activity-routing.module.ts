import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AddActivityComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { StudentListComponent } from './student-list/student-list.component';

const ActivityRoutes: Routes = [
    { path: 'Add-activity', component: AddActivityComponent },
    { path: 'teacher/update-activity/:actId', component: UpdateComponent },
    { path: 'confirm-reject-student', component: StudentListComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(ActivityRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class ActivityRoutingModule {

}
