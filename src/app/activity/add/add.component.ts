import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddActivityComponent implements OnInit {
  form = this.fb.group({
    id: [''],
    name: [''],
    location: [''],
    host: [''],
    startPeriodRegis: [''],
    endPeriodRegis: [''],
    startDate: [''],
    endDate: [''],
    time: [''],
    description: ['']
    
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() { }

  submit() {
    alert("Added!");
  }
  
}
