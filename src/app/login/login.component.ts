import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() { }

  login() {
    var inputEmail = (<HTMLInputElement>document.getElementById('email')).value;
    if (inputEmail == "student") {
      this.router.navigate(['/student/enrollActivity/1']);
    } else if (inputEmail == "teacher") {
      this.router.navigate(['/teacher/waiting-list/1']);
    } else if (inputEmail === "admin") {
      this.router.navigate(['/confirm-reject-student']);
    }
  }

}