import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student/student-service';
import { ActivityService } from './service/activity/activity-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/student/students-file-impl.service';
import { StudentsAddComponent } from './students/add/students.add.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatNativeDateModule
} from '@angular/material';
import { MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { WaitingListComponent } from './teacher/waiting-list/waiting-list.component';
import { TeacherRoutingModule } from './teacher/teacher-routing.module';
import { ActivityRoutingModule } from './activity/activity-routing.module';
import { StudentListComponent } from './activity/student-list/student-list.component';
import { ActivityFileDataImplService } from './service/activity/activity-file-data-impl.service';
import { ViewStudentListComponent } from './teacher/view-student-list/view-student-list.component';
import { ActivityListComponent } from './teacher/activity-list/activity-list.component';
import { TeacherFileDataImplService } from './service/teacher/teacher-file-data-impl.service';
import { TeacherService } from './service/teacher/teacher-service';
import { UpdateComponent } from './activity/update/update.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { UpdateProfileComponent } from './students/update-profile/update-profile.component';
import { DialogActivityDataComponent } from './students/activity-list/dialog-activity-data/dialog-activity-data.component';
import { ActivityListStudentComponent } from './students/activity-list/activity-list.component';
import { ViewEnrolledActivitiesComponent } from './students/view-enrolled-activities/view-enrolled-activities.component';
import { AddActivityComponent } from './activity/add/add.component';
import { LoginComponent } from './login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  declarations: [
    AppComponent,
    WaitingListComponent,
    StudentsAddComponent,
    MyNavComponent,
    FileNotFoundComponent,
    LoginComponent,
    ViewStudentListComponent,
    ActivityListComponent,
    UpdateComponent,
    UpdateProfileComponent,
    DialogActivityDataComponent,
    ViewEnrolledActivitiesComponent,
    StudentListComponent,
    ActivityListStudentComponent,
    AddActivityComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    StudentRoutingModule,
    TeacherRoutingModule,
    ActivityRoutingModule,
    MatDialogModule,
    AppRoutingModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentsFileImplService },
    { provide: ActivityService, useClass: ActivityFileDataImplService },
    { provide: TeacherService, useClass: TeacherFileDataImplService }
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogActivityDataComponent]
})

export class AppModule { }
