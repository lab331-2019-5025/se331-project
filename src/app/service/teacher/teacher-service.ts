import Teacher from 'src/app/entity/teacher';
import { Observable } from '../../../../node_modules/rxjs';

export abstract class TeacherService {
    abstract getTeachers(): Observable<Teacher[]>;
    abstract getTeacherById(id: number): Observable<Teacher>;
}
