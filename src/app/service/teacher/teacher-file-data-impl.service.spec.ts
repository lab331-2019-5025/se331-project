import { TestBed } from '@angular/core/testing';

import { TeacherFileDataImplService } from './teacher-file-data-impl.service';

describe('TeacherFileDataImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeacherFileDataImplService = TestBed.get(TeacherFileDataImplService);
    expect(service).toBeTruthy();
  });
});
