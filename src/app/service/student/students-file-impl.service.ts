import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from 'rxjs';
import Student from 'src/app/entity/student';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class StudentsFileImplService extends StudentService {
  constructor(private http: HttpClient) {
    super();
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student[]>('assets/students.json')
      .pipe(map(students => {
        const output: Student = (students as Student[]).find(student => student.id === +id);
        return output;
      }));

  }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('assets/students.json');
  }
}
