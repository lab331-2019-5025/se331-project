import { Observable } from 'rxjs';
import Student from 'src/app/entity/student';

export abstract class StudentService {
  getStudentById(arg0: any) {
    throw new Error("Method not implemented.");
  }
     abstract getStudents(): Observable<Student[]>;
     abstract getStudent(id: number): Observable<Student>;
}
