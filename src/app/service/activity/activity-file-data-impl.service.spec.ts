import { TestBed } from '@angular/core/testing';

import { ActivityFileDataImplService } from './activity-file-data-impl.service';

describe('ActivityFileDataImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityFileDataImplService = TestBed.get(ActivityFileDataImplService);
    expect(service).toBeTruthy();
  });
});
