import { Observable } from '../../../../node_modules/rxjs';
import Activity from 'src/app/entity/activity';

export abstract class ActivityService {
    abstract getActivities(): Observable<Activity[]>;
    abstract getActivityById(id: number): Observable<Activity>;
}
