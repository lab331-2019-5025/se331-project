import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { PendingTableDataSource } from 'src/app/students/view-enrolled-activities/pending-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params } from '@angular/router';
import { ConfirmTableDataSource } from './confirm-table-datasource';
import { StudentService } from 'src/app/service/student/student-service';
import Student from 'src/app/entity/student';

@Component({
  selector: 'app-view-enrolled-activities',
  templateUrl: './view-enrolled-activities.component.html',
  styleUrls: ['./view-enrolled-activities.component.css']
})
export class ViewEnrolledActivitiesComponent implements OnInit {

  @ViewChild('pendingPaginator', { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('pendingTable', { static: false }) table: MatTable<Activity>;
  dataSourcePending: PendingTableDataSource;

  @ViewChild('confirmPaginator', { static: false }) paginatorC: MatPaginator;
  @ViewChild('confirmTable', { static: false }) tableC: MatTable<Activity>;
  dataSourceConfirm: ConfirmTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'name', 'host', 'date', 'time'];
  filter: string;
  filter$: BehaviorSubject<string>;
  pendingAct: Activity[];
  confirmAct: Activity[];
  student : Student;

  constructor(private acivityService: ActivityService, private studentService: StudentService,private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudent(params['id'])
        .subscribe(student => {
          this.student = student;
          var pending = [];
          var confirm = [];
          this.acivityService.getActivities()
            .subscribe(activities => {
              for (var i = 0; i < activities.length; i++) {
                for (var j = 0; j < student.activities.length; j++){
                  if (student.activities[j].activityId == activities[i].id) {
                    if (student.activities[j].status.toLowerCase() == "confirm"){
                      confirm.push(activities[i]);
                    } else {
                      pending.push(activities[i]);
                    }
                  }
                }
                this.pendingAct = pending;
                this.confirmAct = confirm;
              };
              this.dataSourcePending = new PendingTableDataSource();
              this.dataSourcePending.data = this.pendingAct;
              this.dataSourcePending.sort = this.sort;
              this.dataSourcePending.paginator = this.paginator;
              this.table.dataSource = this.dataSourcePending;
              this.filter$ = new BehaviorSubject<string>('');
              this.dataSourcePending.filter$ = this.filter$;

              this.dataSourceConfirm = new ConfirmTableDataSource();
              this.dataSourceConfirm.data = this.confirmAct;
              this.dataSourceConfirm.sort = this.sort;
              this.dataSourceConfirm.paginator = this.paginatorC;
              this.tableC.dataSource = this.dataSourceConfirm;
              this.filter$ = new BehaviorSubject<string>('');
              this.dataSourceConfirm.filter$ = this.filter$;
            });
        });
    });
  }


  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }


}
