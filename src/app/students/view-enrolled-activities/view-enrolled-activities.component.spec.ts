import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEnrolledActivitiesComponent } from './view-enrolled-activities.component';

describe('ViewEnrolledActivitiesComponent', () => {
  let component: ViewEnrolledActivitiesComponent;
  let fixture: ComponentFixture<ViewEnrolledActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEnrolledActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEnrolledActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
