import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivityTableDataSource } from './activity-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { MatDialog } from '@angular/material';
import { DialogActivityDataComponent } from './dialog-activity-data/dialog-activity-data.component';
import Activity from 'src/app/entity/activity';


@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})

export class ActivityListStudentComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'host', 'periodRegis', 'date', 'time', 'location', 'btnCol'];
  activities: Activity[];
  activity: Activity;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private activityService: ActivityService, public dialog: MatDialog) { }

  ngOnInit() {
    this.activityService.getActivities()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  openActivityDetail(activities: Activity) {
    const dialogRef = this.dialog.open(DialogActivityDataComponent, {
      data: activities
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result == true) {
        alert("Enrollment complete");
      }
    });
  }

}


