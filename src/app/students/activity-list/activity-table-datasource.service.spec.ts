import { TestBed } from '@angular/core/testing';

import { ActivityTableDataSource } from './activity-table-datasource';

describe('ActivityTableDatasourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityTableDataSource = TestBed.get(ActivityTableDataSource);
    expect(service).toBeTruthy();
  });
});
