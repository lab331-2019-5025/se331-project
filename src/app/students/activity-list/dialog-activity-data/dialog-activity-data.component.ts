import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-dialog-activity-data',
  templateUrl: './dialog-activity-data.component.html',
  styleUrls: ['./dialog-activity-data.component.css']
})

export class DialogActivityDataComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogActivityDataComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Activity) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() { }

}
