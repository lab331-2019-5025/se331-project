import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from 'src/app/service/student/student-service';
import Student from 'src/app/entity/student';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})

export class UpdateProfileComponent implements OnInit {
  student: Student;

  form = this.fb.group({
    id: [''],
    name: [''],
    surname: [''],
    dob: [''],
    studentId: [''],
    major: [''],
    year: [''],
    email: [''],
    image: [''],
    username: [''],
    password: ['']
  });

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private studentService: StudentService) { }

  ngOnInit(): void {
    this.route.params
      .subscribe((params: Params) => {
        this.studentService.getStudent(+params['id'])
          .subscribe(student => {
            this.student = student;
            this.form = this.fb.group({
              id: student.id,
              name: student.name,
              surname: student.surname,
              dob: student.dob,
              studentId: student.studentId,
              major: student.major,
              year: student.year,
              email: student.email,
              image: student.image,
              username: student.username,
              password: student.password
            });
          });
      });
  }

  updateProfile() {
    alert("Updated");
  }

}
