import { Component } from '@angular/core';
import Student from '../../entity/student';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})

export class StudentsAddComponent {
  students: Student[];

  form = this.fb.group({
    id: [''],
    name: [''],
    surname: [''],
    dob: [''],
    studentId: [''],
    major: [''],
    year: [''],
    email: [''],
    image: [''],
    username: [''],
    password: ['']
  });

  constructor(private fb: FormBuilder, private router: Router) { }

  submit() {
      alert("Registration complete");
      this.router.navigate(['/login']);
  }

}
