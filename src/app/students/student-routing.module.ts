import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ActivityListStudentComponent } from './activity-list/activity-list.component';
import { ViewEnrolledActivitiesComponent } from './view-enrolled-activities/view-enrolled-activities.component';
import { StudentsAddComponent } from './add/students.add.component';

const StudentRoutes: Routes = [
    { path: 'student/enrollActivity/:id', component: ActivityListStudentComponent },
    { path: 'student/registration', component: StudentsAddComponent },
    { path: 'student/update_profile/:id', component: UpdateProfileComponent },
    { path: 'student/view-enrolled-act/:id', component: ViewEnrolledActivitiesComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class StudentRoutingModule {

}
