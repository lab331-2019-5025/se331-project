import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) { }

  isShowMenu() {
    if (window.innerWidth < 780) {
      return true;
    } else {
      return false;
    }
  }

  isShowNav() {
    if (window.location.pathname === "/login") {
      return false;
    } else {
      return true;
    }
  }

  clicked() { }

  isStudent() {
    var path = window.location.pathname.substring(0, window.location.pathname.length - 1);
    if (path == "/student/enrollActivity/" || path == "/student/update_profile/" || path == "/student/view-enrolled-act/") {
      return true;
    } else {
      return false;
    }
  }

  isTeacher() {
    var path = window.location.pathname.substring(0, window.location.pathname.length - 1);
    if (path == "/teacher/waiting-list/" || path == "/teacher/view-student-list/" || path == "/teacher/activity-list/" || path == "/teacher/update-activity/") {
      return true;
    } else {
      return false;
    }
  }

  isAdmin() {
    var path = window.location.pathname;
    if (path == "/Add-activity" || path == "/confirm-reject-student") {
      return true;
    } else {
      return false;
    }
  }

  isRegis() {
    if (window.location.pathname === "/student/registration") {
      return true;
    } else {
      return false;
    }
  }

}
