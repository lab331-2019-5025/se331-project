export default class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    periodRegis: string;
    date: string;
    startPeriodRegis: string;
    endPeriodRegis: string;
    startDate: string;
    endDate: string;
    time: string;
    host: string;
    hostId: number;
    students: number[];
}
