export default class Teacher {
    id: number;
    name: string;
    surname: string;
    activities: number[];
}
