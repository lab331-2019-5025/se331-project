import { Status } from './status';

export default class Student {
  id: number;
  name: string;
  surname: string;
  studentId: string;
  dob: string;
  major: string;
  year: number;
  email: string;
  image: string;
  username: string;
  password: string;
  activities: Status[];
  status: string;
  confirmed: boolean; 
}
