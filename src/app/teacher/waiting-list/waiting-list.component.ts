import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-waiting-list',
  templateUrl: './waiting-list.component.html',
  styleUrls: ['./waiting-list.component.css']
})

export class WaitingListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'name', 'date', 'time', 'viewBtn'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private acivityService: ActivityService) { }

  ngOnInit() {
    this.acivityService.getActivities()
      .subscribe(actList => {
        this.route.params
          .subscribe((params: Params) => {
            var output = [];
            for (var i = 0; i < actList.length; i++) {
              if (+actList[i].hostId === +params['hostId']) {
                output.push(actList[i]);
              }
            };
            this.dataSource = new ActivityTableDataSource();
            this.dataSource.data = output;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = this.dataSource;
            this.activities = output;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSource.filter$ = this.filter$;
          });
      });
  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
