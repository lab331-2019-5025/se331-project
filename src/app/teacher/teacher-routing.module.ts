import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { WaitingListComponent } from './waiting-list/waiting-list.component';
import { ViewStudentListComponent } from './view-student-list/view-student-list.component';
import { ActivityListComponent } from './activity-list/activity-list.component';

const TeacherRoutes: Routes = [
    { path: 'teacher/waiting-list/:hostId', component: WaitingListComponent },
    { path: 'teacher/view-student-list/:actId', component: ViewStudentListComponent },
    { path: 'teacher/activity-list/:teacherId', component: ActivityListComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(TeacherRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TeacherRoutingModule {

}
