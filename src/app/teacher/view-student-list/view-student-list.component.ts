import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { StudentTableDataSource } from './student-table-datasource';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from 'src/app/service/student/student-service';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-student-list',
  templateUrl: './view-student-list.component.html',
  styleUrls: ['./view-student-list.component.css']
})

export class ViewStudentListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['studentId', 'name', 'surname', 'major', 'year', 'btnCol'];
  students: Student[];
  actName: string;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private studentService: StudentService, private acivityService: ActivityService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.acivityService.getActivityById(params['actId'])
          .subscribe(activity => {
            var studentList = [];
            this.studentService.getStudents()
              .subscribe(students => {
                for (var i = 0; i < students.length; i++) {
                  if (activity.students.includes(students[i].id)) {
                    studentList.push(students[i]);
                  }
                };
                this.actName = activity.name;
                this.dataSource = new StudentTableDataSource();
                this.dataSource.data = studentList;
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
                this.table.dataSource = this.dataSource;
                this.students = studentList;
                this.filter$ = new BehaviorSubject<string>('');
                this.dataSource.filter$ = this.filter$;
              });
          });
      });
  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  get getActName() {
    return this.actName;
  }

  confirm(student: Student) {
    console.log(student.id + " " + student.name + " confirmed!");
  }

  reject(student: Student) {
    console.log(student.id + " " + student.name + " rejected!");
  }

}
