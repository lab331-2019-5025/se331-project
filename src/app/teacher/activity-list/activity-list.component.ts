import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { TeacherService } from 'src/app/service/teacher/teacher-service';
import Teacher from 'src/app/entity/teacher';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'name', 'date', 'time', 'location', 'periodRegis', 'btnCol'];
  activities: Activity[];
  teacher: Teacher;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private acivityService: ActivityService, private teacherService: TeacherService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.teacherService.getTeacherById(params['teacherId'])
          .subscribe(teacher => {
            this.teacher = teacher;
            var activityList = [];
            this.acivityService.getActivities()
              .subscribe(activities => {
                for (var i = 0; i < activities.length; i++) {
                  if (teacher.activities.includes(activities[i].id)) {
                    activityList.push(activities[i]);
                  }
                };
                this.dataSource = new ActivityTableDataSource();
                this.dataSource.data = activityList;
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
                this.table.dataSource = this.dataSource;
                this.activities = activityList;
                this.filter$ = new BehaviorSubject<string>('');
                this.dataSource.filter$ = this.filter$;
              });
          });
      });
  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  remove(activity: Activity) {
    console.log("Remove activity id " + activity.id);
  }

}
